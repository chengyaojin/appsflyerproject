using AppsFlyerSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testUI : MonoBehaviour
{
    public Button btn1;
    public Button btn2;
    Dictionary<string, string> paramsDic = new Dictionary<string, string>();
    // Start is called before the first frame update
    void Start()
    {
        btn1.onClick.AddListener(OnClickEv_1);
        btn2.onClick.AddListener(OnClickEv_2);
    }
    void OnClickEv_1()
    {
        //paramsDic.Clear();
        //paramsDic.Add(AFInAppEvents.PARAM_1,"faceBook");
        //AppsFlyer.sendEvent(AFInAppEvents.LOGIN, paramsDic);

        System.Collections.Generic.Dictionary<string, string> purchaseEvent = new
        System.Collections.Generic.Dictionary<string, string>();
        purchaseEvent.Add(AFInAppEvents.CURRENCY, "USD");
        purchaseEvent.Add(AFInAppEvents.REVENUE, "-200");
        purchaseEvent.Add(AFInAppEvents.QUANTITY, "1");
        purchaseEvent.Add(AFInAppEvents.CONTENT_TYPE, "category_a");
        AppsFlyer.sendEvent("cancel_purchase", purchaseEvent);
        Debug.LogError("shengming click 1");
    }
    void OnClickEv_2()
    {
        paramsDic.Clear();
        paramsDic.Add("shengming", "faceBook");
        AppsFlyer.sendEvent(AFInAppEvents.LOGIN, paramsDic);
        Debug.LogError("shengming click 2");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
